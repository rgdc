/*
 *  Copyright (C) 2008 by Andrei Alexandrescu
 *  Written by Andrei Alexandrescu, www.erdani.org
 *  Based on an idea by Georg Wrede
 *  Featuring improvements suggested by Christopher Wright
 *  Windows port using bug fixes and suggestions by Adam Ruppe
 *
 *  Code uglification: Ketmar // Invisible Vector
 *
 * Distributed under the Boost Software License, Version 1.0.
 *    (See accompanying file LICENSE_1_0.txt or copy at
 *          http://www.boost.org/LICENSE_1_0.txt)
 */
// -fversion=no_pragma_lib to disable non-standard -fwrite-pragma-lib arg

import
  std.c.stdlib,
  std.algorithm,
  std.array,
  std.datetime,
  std.digest.md,
  std.exception,
  std.file,
  std.getopt,
  std.parallelism,
  std.path,
  std.process,
  std.range,
  std.regex,
  std.stdio,
  std.string,
  std.typetuple;


private enum objExt = ".o";
private enum binExt = "";
private enum libExt = ".a";
private enum altDirSeparator = "";

private bool chatty, buildOnly, dryRun, force;
private string exe;


private string compilerBinary = "gdc";
private string[] compilerExtraFlags = ["-pipe"];

private bool optDynamic = false;
private bool optShowCommands = false;
private bool optRunTiming = false;
private bool optCompileTiming = false;


////////////////////////////////////////////////////////////////////////////////
private void yap(size_t line=__LINE__, T...) (auto ref T stuff) {
  if (!chatty) return;
  debug stderr.writeln(line, ": ", stuff);
  else stderr.writeln(stuff);
}


////////////////////////////////////////////////////////////////////////////////
private string[] includeDirs; // -I
private string[] importDirs; // -J
private bool[string] systemDirs; // to ignore


private string expandString (string s) {
  auto pos = s.indexOf("$(");
  if (pos >= 0) {
    string res;
    while (pos >= 0) {
      res ~= s[0..pos];
      s = s[pos+2..$];
      auto epos = s.indexOf(')');
      if (epos < 1) throw new Exception("invalid envvar name");
      auto v = environment.get(s[0..epos], "");
      res ~= v;
      s = s[epos+1..$];
      pos = s.indexOf("$(");
    }
    res ~= s;
    return res;
  }
  return s;
}


private void readRC (string fname) {
  auto reader = File(fname);
  scope(exit) collectException(reader.close()); // don't care for errors
  foreach (string line; lines(reader)) {
    if (line.startsWith("include=")) {
      auto val = expandString(line[8..$].strip);
      val = buildNormalizedPath(val);
      if (val.length > 0) {
        val = "-I"~val;
        if (!includeDirs.canFind(val)) includeDirs ~= val;
      }
    } else if (line.startsWith("import=")) {
      auto val = expandString(line[7..$].strip);
      val = buildNormalizedPath(val);
      if (val.length > 0) {
        val = "-J"~val;
        if (!importDirs.canFind(val)) importDirs ~= val;
      }
    } else if (line.startsWith("systemdir=")) {
      auto val = expandString(line[10..$].strip);
      val = buildNormalizedPath(val);
      if (val.length > 0) systemDirs[val] = true;
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
private File lockFile;

private void lockWorkPath (string workPath) {
  string lockFileName = buildPath(workPath, "rgdc.lock");
  if (!dryRun) lockFile.open(lockFileName, "w");
  yap("lock ", lockFile.name);
  if (!dryRun) lockFile.lock();
}


private void unlockWorkPath () {
  yap("unlock ", lockFile.name);
  if (!dryRun) {
    lockFile.unlock();
    lockFile.close();
  }
}


////////////////////////////////////////////////////////////////////////////////
// Run a program optionally writing the command line first
// If "replace" is true and the OS supports it, replace the current process.
private int run (string[] args, string output=null, bool replace=false) {
  import std.conv;
  yap(replace ? "exec " : "spawn ", args.text);
  if (optShowCommands) stderr.writeln("run: ", args.join(" ")); //TODO: proper quoting
  if (dryRun) return 0;
  if (replace && !output) {
    import std.c.process;
    auto argv = args.map!toStringz.chain(null.only).array;
    return execv(argv[0], argv.ptr);
  }
  File outputFile;
  if (output) {
    outputFile = File(output, "wb");
  } else {
    outputFile = stdout;
  }
  auto process = spawnProcess(args, stdin, outputFile);
  return process.wait();
}


private int runSilent (string[] args) {
  import std.conv;
  yap("spawn ", args.text);
  if (optShowCommands) stderr.writeln("run: ", args.join(" ")); //TODO: proper quoting
  if (dryRun) return 0;
  auto process = spawnProcess(args);
  return process.wait();
}


private int exec (string[] args) {
  return run(args, null, true);
}


////////////////////////////////////////////////////////////////////////////////
private @property string myOwnTmpDir () {
  import core.sys.posix.unistd;
  auto tmpRoot = format("/tmp/.rgdc-%d", getuid());
  yap("mkdirRecurse ", tmpRoot);
  if (!dryRun) mkdirRecurse(tmpRoot);
  return tmpRoot;
}


private string getWorkPath (in string root, in string[] compilerFlags) {
  static string workPath;
  if (workPath) return workPath;
  enum string[] irrelevantSwitches = ["--help", "-ignore", "-quiet", "-v"];
  MD5 context;
  context.start();
  context.put(root.absolutePath().representation);
  foreach (flag; compilerFlags) {
    if (irrelevantSwitches.canFind(flag)) continue;
    context.put(flag.representation);
  }
  auto digest = context.finish();
  string hash = toHexString(digest);
  const tmpRoot = myOwnTmpDir;
  workPath = buildPath(tmpRoot, "rgdc-"~baseName(root)~'-'~hash);
  yap("mkdirRecurse ", workPath);
  if (!dryRun) mkdirRecurse(workPath);
  return workPath;
}


////////////////////////////////////////////////////////////////////////////////
// Rebuild the executable fullExe starting from modules in myDeps
// passing the compiler flags compilerFlags. Generates one large
// object file.
private int rebuild (string root, string fullExe,
                     string workDir, string objDir, in string[string] myDeps,
                     string[] compilerFlags, bool addStubMain)
{
  // Delete the old executable before we start building.
  yap("stat ", fullExe);
  if (!dryRun && exists(fullExe)) {
    yap("rm ", fullExe);
    try {
      remove(fullExe);
    } catch (FileException e) {
      // This can occur on Windows if the executable is locked.
      // Although we can't delete the file, we can still rename it.
      auto oldExe = "%s.%s-%s.old".format(fullExe, Clock.currTime.stdTime, thisProcessID);
      yap("mv ", fullExe, " ", oldExe);
      rename(fullExe, oldExe);
    }
  }
  auto fullExeTemp = fullExe~".tmp";

  string[] buildTodo () {
    string outExe = fullExeTemp;//(fullExeTemp[0] != '/' ? objDir~"/"~fullExeTemp : fullExeTemp);
    auto todo =
      compilerFlags~
      ["-o", outExe]~
      //["-I"~dirName(root)]~
      //["-J"~dirName(root)]~
      includeDirs~
      importDirs/*~
      [root]*/;
    foreach (k, objectFile; myDeps) if (objectFile !is null) todo ~= [k];
    // Need to add void main(){}?
    if (addStubMain) {
      auto stubMain = buildPath(myOwnTmpDir, "stubmain.d");
      std.file.write(stubMain, "void main(){}");
      todo ~= [stubMain];
    }
    return todo;
  }

  auto todo = buildTodo();
  auto commandLength = escapeShellCommand(todo).length;
  string[] eflags2;
  if (optDynamic) {
    eflags2 ~= "-fPIC";
  } else {
    eflags2 ~= "-static-libphobos";
  }
  // get list of pragma(lib)s
  string[] prlibs;
  version(no_pragma_lib) {} else {
    string prfname = objDir~"/"~fullExeTemp.baseName~".link";
    //writeln(prfname);
    collectException(remove(prfname));
    string[] args =
      [compilerBinary]~
      compilerExtraFlags~
      ["-c", "-o", "/dev/null"]~
      ["-fwrite-pragma-libs="~prfname]~
      todo;
    auto af = args.filter!(a => !a.startsWith("-O") && !a.startsWith("-finline-"));
    args.length = 0;
    while (!af.empty) {
      auto s = af.front;
      af.popFront();
      if (s == "-o") {
        s = af.front;
        af.popFront();
        if (s == "/dev/null") args ~= ["-o", "/dev/null"];
      } else {
        args ~= s;
      }
    }
    //args = array(args.filter!(a => !a.startsWith("-O") && !a.startsWith("-finline-")));
    //writeln(" *** ", args);
    immutable r1 = run(args);
    try {
      auto reader = File(prfname, "r");
      foreach (string line; lines(reader)) {
        auto s = line.strip;
        if (!prlibs.canFind(s)) prlibs ~= s.idup;
      }
    } catch (Exception) {}
    if (prlibs.length) {
      try {
        auto fo = File(prfname, "w");
        foreach (s; prlibs) fo.writeln(s);
      } catch (Exception) {
        prfname = null;
      }
    } else {
      prfname = null;
    }
    if (prfname.length > 0) {
      prlibs = ["-Wl,@"~prfname];
    } else {
      prlibs.length = 0;
    }
  }
  if (commandLength+compilerBinary.length+compilerExtraFlags.join(" ").length+eflags2.join(" ").length+
      prlibs.join(" ").length > 32700) throw new Exception("SHIT!");
  Timer btm;
  btm.start();
  immutable result = run([compilerBinary]~compilerExtraFlags~eflags2~todo~prlibs);
  btm.stop();
  if (result) {
    // build failed
    if (exists(fullExeTemp)) remove(fullExeTemp);
    return result;
  }
  if (!dryRun && optCompileTiming) writeln("RGDC: compile time: ", btm);
  // clean up the dir containing the object file, just not in dry
  // run mode because we haven't created any!
  if (!dryRun) {
    yap("stat ", objDir);
    if (objDir.exists && objDir.startsWith(workDir)) {
      yap("rmdirRecurse ", objDir);
      // We swallow the exception because of a potential race: two
      // concurrently-running scripts may attempt to remove this
      // directory. One will fail.
      collectException(rmdirRecurse(objDir));
    }
    yap("mv ", fullExeTemp, " ", fullExe);
    rename(fullExeTemp, fullExe);
  }
  return 0;
}


////////////////////////////////////////////////////////////////////////////////
// we don't need std exclusions anymore, we have 'systemdir' key in .rc file for that
//private string[] exclusions = ["std", "etc", "core", "tango"]; // packages that are to be excluded
private string[] exclusions = []; // packages that are to be excluded


// Given module rootModule, returns a mapping of all dependees .d
// source filenames to their corresponding .o files sitting in
// directory workDir. The mapping is obtained by running dmd -v against
// rootModule.
private string[string] getDependencies (string rootModule, string workDir, string objDir, string[] compilerFlags) {
  immutable depsFilename = buildPath(workDir, "rgdc.deps");

  string[string] readDepsFile (/*string depsFilename, string objDir="."*/) {

    bool[string] sysDirCache; // cache all modules that are in systemdirs

    bool inALibrary (string source, string object) {
      if (object.endsWith(".di") || source == "object" || source == "gcstats") return true;
      foreach (string exclusion; exclusions) if (source.startsWith(exclusion~'.')) return true;
      return false;
    }

    bool isInSystemDir (string path) {
      path = buildNormalizedPath(path);
      if (path in sysDirCache) return true;
      //writeln("isInSystemDir: path=", path, "; dirs=", systemDirs);
      foreach (auto k; systemDirs.byKey) {
        if (path.length > k.length && path.startsWith(k) && path[k.length] == '/') {
          sysDirCache[path.idup] = true;
          //writeln("in system dir: ", path);
          return true;
        }
      }
      return false;
    }

    string d2obj (string dfile) { return buildPath(objDir, dfile.baseName.chomp(".d")~objExt); }

    string findLib (string libName) {
      // This can't be 100% precise without knowing exactly where the linker
      // will look for libraries (which requires, but is not limited to,
      // parsing the linker's command line (as specified in dmd.conf/sc.ini).
      // Go for best-effort instead.
      string[] dirs = ["."];
      foreach (envVar; ["LIB", "LIBRARY_PATH", "LD_LIBRARY_PATH"]) dirs ~= environment.get(envVar, "").split(pathSeparator);
      string[] names = ["lib"~libName~".so", "lib"~libName~".a"];
      dirs ~= ["/lib", "/usr/lib", "/usr/local/lib"];
      foreach (dir; dirs) {
        foreach (name; names) {
          auto path = buildPath(dir, name);
          if (path.exists) {
            if (path.extension == ".so") return "-l"~path.baseName.stripExtension[3..$];
            return absolutePath(path);
          }
        }
      }
      return null;
    }

    yap("read ", depsFilename);
    auto depsReader = File(depsFilename);
    scope(exit) collectException(depsReader.close()); // don't care for errors
    // Fetch all dependencies and append them to myDeps
    static auto modInfoRE = ctRegex!(r"^(.+?)\s+\(([^)]+)\)");
    //std.stdio (/opt/gdc/include/d/4.9.1/std/stdio.d) : private : object (/opt/gdc/include/d/4.9.1/object.di)
    static auto modImportRE = ctRegex!(r"^(.+?)\s+\(([^)]+)\)\s+:\s+string\s+:\s+[^(]+\(([^)]+)\)");
    //hello (hello.d) : string : zm (/mnt/tigerclaw/D/prj/rgdc/rgdc_native/zm)
    string[string] result;
    foreach (string line; lines(depsReader)) {
      string modName, modPath, filePath;
      auto modImportMatch = match(line, modImportRE);
      if (modImportMatch.empty) {
        auto modInfoMatch = match(line, modInfoRE);
        if (modInfoMatch.empty) continue;
        auto modInfoCaps = modInfoMatch.captures;
        // [1]: module name
        // [2]: module path
        modName = modInfoCaps[1];
        modPath = modInfoCaps[2];
      } else {
        auto modImportCaps = modImportMatch.captures;
        // [1]: module name
        // [2]: module path
        // [3]: file path
        modName = modImportCaps[1];
        modPath = modImportCaps[2];
        filePath = modImportCaps[3];
      }
      if (filePath.length && !isInSystemDir(filePath)) result[filePath] = null;
      //if (inALibrary(modName, modPath) || isInSystemDir(modPath)) continue;
      if (isInSystemDir(modPath)) continue;
      result[modPath] = d2obj(modPath);
    }
    return result;
  }

  // Check if the old dependency file is fine
  if (!force) {
    yap("stat ", depsFilename);
    if (exists(depsFilename)) {
      // See if the deps file is still in good shape
      auto deps = readDepsFile();
      auto allDeps = chain(rootModule.only, deps.byKey).array;
      bool mustRebuildDeps = allDeps.anyNewerThan(timeLastModified(depsFilename));
      if (!mustRebuildDeps) return deps; // Cool, we're in good shape
      // Fall through to rebuilding the deps file
    }
  }

  immutable rootDir = dirName(rootModule);

  // Collect dependencies
  auto depsGetter =
      // "cd "~shellQuote(rootDir)~" && "
      [compilerBinary]~compilerExtraFlags~compilerFlags~
      [
       "-c",
       "-o", "/dev/null",
       "-fdeps="~depsFilename,
       rootModule,
       //"-I"~rootDir,
       //"-J"~rootDir
      ]~includeDirs~importDirs;

  scope(failure) {
    // Delete the deps file on failure, we don't want to be fooled
    // by it next time we try
    collectException(std.file.remove(depsFilename));
  }

  immutable depsExitCode = runSilent(depsGetter);
  if (depsExitCode) {
    stderr.writefln("Failed: %s", depsGetter);
    collectException(std.file.remove(depsFilename));
    exit(depsExitCode);
  }

  return (dryRun ? null : readDepsFile());
}


////////////////////////////////////////////////////////////////////////////////
// Is any file newer than the given file?
private bool anyNewerThan (in string[] files, in string file) {
  yap("stat ", file);
  return files.anyNewerThan(file.timeLastModified);
}


// Is any file newer than the given file?
private bool anyNewerThan (in string[] files, SysTime t) {
  // Experimental: running newerThan in separate threads, one per file
  if (false) {
    foreach (source; files) if (source.newerThan(t)) return true;
    return false;
  } else {
    bool result;
    foreach (source; taskPool.parallel(files)) if (!result && source.newerThan(t)) result = true;
    return result;
  }
}


/*
 * If force is true, returns true. Otherwise, if source and target both
 * exist, returns true iff source's timeLastModified is strictly greater
 * than target's. Otherwise, returns true.
 */
private bool newerThan (string source, string target) {
  if (force) return true;
  yap("stat ", target);
  return source.newerThan(timeLastModified(target, SysTime(0)));
}


private bool newerThan (string source, SysTime target) {
  if (force) return true;
  try {
    yap("stat ", source);
    return DirEntry(source).timeLastModified > target;
  } catch (Exception) {
    // File not there, consider it newer
    return true;
  }
}


////////////////////////////////////////////////////////////////////////////////
private @property string helpString () {
    return
"rgdc build "~thisVersion~"
Usage: rgdc [RDMD AND DMD OPTIONS]... program [PROGRAM OPTIONS]...
Builds (with dependents) and runs a D program.
Example: rgdc -release myprog --myprogparm 5

Any option to be passed to the compiler must occur before the program name. In
addition to compiler options, rgdc recognizes the following options:
  --build-only       just build the executable, don't run it
  --chatty           write compiler commands to stdout before executing them
  --compiler=comp    use the specified compiler
  --dry-run          do not compile, just show what commands would be run (implies --chatty)
  --exclude=package  exclude a package from the build (multiple --exclude allowed)
  --force            force a rebuild even if apparently not necessary
  --help             this message
  --main             add a stub main program to the mix (e.g. for unittesting)
  --shebang          rgdc is in a shebang line (put as first argument)
  --time             measure program executing time
  --compile-time     measure compile time
";
}


////////////////////////////////////////////////////////////////////////////////
private @property string thisVersion () {
  enum d = __DATE__;
  enum month = d[0..3],
      day = d[4] == ' ' ? "0"~d[5] : d[4..6],
      year = d[7..$];
  enum monthNum
      = month == "Jan" ? "01"
      : month == "Feb" ? "02"
      : month == "Mar" ? "03"
      : month == "Apr" ? "04"
      : month == "May" ? "05"
      : month == "Jun" ? "06"
      : month == "Jul" ? "07"
      : month == "Aug" ? "08"
      : month == "Sep" ? "09"
      : month == "Oct" ? "10"
      : month == "Nov" ? "11"
      : month == "Dec" ? "12"
      : "";
  static assert(month != "", "Unknown month "~month);
  return year[0]~year[1..$]~monthNum~day;
}


private string which (string path) {
  yap("which ", path);
  if (path.canFind(dirSeparator) || altDirSeparator != "" && path.canFind(altDirSeparator)) return path;
  string[] extensions = [""];
  foreach (extension; extensions) {
    foreach (envPath; environment["PATH"].splitter(pathSeparator)) {
      string absPath = buildPath(envPath, path~extension);
      yap("stat ", absPath);
      if (exists(absPath) && isFile(absPath)) return absPath;
    }
  }
  throw new FileException(path, "File not found in PATH");
}


private size_t indexOfProgram (string[] args) {
  foreach(i, arg; args[1..$]) {
    if (!arg.startsWith('-', '@') && !arg.endsWith(".o", ".a")) return i+1;
  }
  return args.length;
}


int main(string[] args) {

  string[] convertSomeOptions (string[] args, string root) {
    string[string] simpleReplace;
    simpleReplace["-unittest"] = "-funittest";
    simpleReplace["-main"] = "--main";
    simpleReplace["-time"] = "--time";
    simpleReplace["-compile-time"] = "--compile-time";
    simpleReplace["-O"] = "-O2";
    simpleReplace["-boundscheck=off"] = "-fno-bounds-check";
    simpleReplace["-boundscheck=on"] = "-fbounds-check";

    bool hasIDot, hasJDot;
    int idxI = -1, idxJ = -1;
    string rootDir = root.dirName.buildNormalizedPath;
    if (rootDir.length == 0) rootDir ~= ".";
    string ourI = "-I"~rootDir;
    string ourJ = "-J"~rootDir;

    debug writeln("before: ", args);
    string[] res;
    res ~= args[0];
    res ~= "-fignore-unknown-pragmas";
    foreach (arg; args[1..$]) {
      auto sr = arg in simpleReplace;
      if (sr !is null) { res ~= *sr; continue; }
      if (arg.startsWith("-debug") ||
          arg.startsWith("-version")) {
        res ~= "-f"~arg[1..$];
        continue;
      }
      if (arg == "-inline") { res ~= ["-finline-small-functions", "-finline-functions"]; continue; }
      if (arg == ourI) hasIDot = true;
      else if (arg == ourJ) hasJDot = true;
      if (idxI < 0 && arg.startsWith("-I")) idxI = cast(int)res.length;
      else if (idxJ < 0 && arg.startsWith("-J")) idxJ = cast(int)res.length;
      res ~= arg;
    }
    if (!res.canFind("-g") && !res.canFind("-s")) res = res~"-s";
    if (!hasIDot) {
      if (idxI < 0) idxI = res.length;
      res = res[0..idxI]~[ourI]~res[idxI..$];
    }
    if (!hasJDot) {
      if (idxJ < 0) idxJ = res.length;
      res = res[0..idxJ]~[ourJ]~res[idxJ..$];
    }
    debug writeln("after: ", res);
    return res;
  }

  //writeln("Invoked with: ", args);
  if (args.length > 1 && args[1].startsWith("--shebang ", "--shebang=")) {
    // multiple options wrapped in one
    auto a = args[1]["--shebang ".length..$];
    args = args[0..1]~std.string.split(a)~args[2..$];
  }

  // Continue parsing the command line; now get rgdc's own arguments
  auto programPos = indexOfProgram(args);
  assert(programPos > 0);
  auto argsBeforeProgram = convertSomeOptions(args[0..programPos], (programPos < args.length ? args[programPos] : null));

  bool bailout; // bailout set by functions called in getopt if program should exit
  bool addStubMain; // set by --main
  getopt(argsBeforeProgram,
    std.getopt.config.caseSensitive,
    std.getopt.config.passThrough,
    "build-only", &buildOnly,
    "chatty", &chatty,
    "compiler", &compilerBinary,
    "dry-run", &dryRun,
    "exclude", &exclusions,
    "force", &force,
    "help", { writeln(helpString); bailout = true; },
    "main", &addStubMain,
    "show-commands", &optShowCommands,
    "dynamic", &optDynamic,
    "static", { optDynamic = false; },
    "time", &optRunTiming,
    "compile-time", &optCompileTiming
  );
  if (bailout) return 0;
  if (dryRun) chatty = true; // dry-run implies chatty

  // no code on command line => require a source file
  if (programPos == args.length) {
    write(helpString);
    return 1;
  }

  auto
    root = args[programPos].chomp(".d")~".d",
    exeBasename = root.baseName(".d"),
    exeDirname = root.dirName,
    programArgs = args[programPos+1..$];

  assert(argsBeforeProgram.length >= 1);
  auto compilerFlags = argsBeforeProgram[1..$];

  //TODO
  bool lib = compilerFlags.canFind("-lib");
  string outExt = (lib ? libExt : binExt);

  /*
  if (!optDynamic && compilerFlags.canFind("-Os") && !compilerFlags.canFind("-Wl,--gc-sections")) {
    compilerFlags ~= "-Wl,--gc-sections";
  }
  */

  //collectException(readRC(thisExePath.setExtension(".rc")));
  foreach (fname; ["./rgdc.rc", "$(HOME)/.rgdc.rc", "/etc/rgdc.rc", "!"]) {
    if (fname == "!") fname = thisExePath.setExtension(".rc");
    else fname = expandString(fname);
    try {
      //writeln("trying '", fname, "'");
      readRC(fname);
      break;
    } catch (Exception e) {
      //writeln(" FAILED! ", e.msg);
    }
  }

  // --build-only implies the user would like a binary in the program's directory
  if (buildOnly && !exe) exe = exeDirname~dirSeparator;

  if (exe && exe.endsWith(dirSeparator)) {
    // user specified a directory, complete it to a file
    exe = buildPath(exe, exeBasename)~outExt;
  }

  // Compute the object directory and ensure it exists
  immutable workDir = getWorkPath(root, compilerFlags);
  lockWorkPath(workDir); // will be released by the OS on process exit
  string objDir = buildPath(workDir, "objs");
  yap("mkdirRecurse ", objDir);
  if (!dryRun) mkdirRecurse(objDir);

  if (lib) {
    // When building libraries, DMD does not generate object files.
    // Instead, it uses the -od parameter as the location for the library file.
    // Thus, override objDir (which is normally a temporary directory)
    // to be the target output directory.
    objDir = exe.dirName;
  }

  // Fetch dependencies
  const myDeps = getDependencies(root, workDir, objDir, compilerFlags);

  // Compute executable name, check for freshness, rebuild
  /*
    We need to be careful about using -o. Normally the generated
    executable is hidden in the unique directory workDir. But if the
    user forces generation in a specific place by using -od or -of,
    the time of the binary can't be used to check for freshness
    because the user may change e.g. the compile option from one run
    to the next, yet the generated binary's datetime stays the
    same. In those cases, we'll use a dedicated file called ".built"
    and placed in workDir. Upon a successful build, ".built" will be
    touched. See also
    http://d.puremagic.com/issues/show_bug.cgi?id=4814
   */
  string buildWitness;
  SysTime lastBuildTime = SysTime.min;
  if (exe) {
    // user-specified exe name
    buildWitness = buildPath(workDir, ".built");
    if (!exe.newerThan(buildWitness)) {
      // Both exe and buildWitness exist, and exe is older than
      // buildWitness. This is the only situation in which we
      // may NOT need to recompile.
      lastBuildTime = buildWitness.timeLastModified(SysTime.min);
    }
  } else {
    exe = buildPath(workDir, exeBasename)~outExt;
    buildWitness = exe;
    lastBuildTime = buildWitness.timeLastModified(SysTime.min);
  }

  // Have at it
  if (chain(root.only, myDeps.byKey).array.anyNewerThan(lastBuildTime)) {
    immutable result = rebuild(root, exe, workDir, objDir, myDeps, compilerFlags, addStubMain);
    if (result) return result;
    // Touch the build witness to track the build time
    if (buildWitness != exe) {
      yap("touch ", buildWitness);
      std.file.write(buildWitness, "");
    }
  }

  if (buildOnly) {
    // Pretty much done!
    return 0;
  }

  // release lock on workDir before launching the user's program
  unlockWorkPath();

  // run
  if (!dryRun && optRunTiming) {
    Timer rtm;
    rtm.start();
    int res = run(exe~programArgs);
    rtm.stop();
    writeln("RGDC: execution time: ", rtm);
    return res;
  }
  return exec(exe~programArgs);
}


////////////////////////////////////////////////////////////////////////////////
struct Timer {
  import core.time;

  enum State {
    Stopped,
    Running,
    Paused
  }

  @property auto state () @safe const nothrow { return mState; }

  @property bool stopped () @safe const nothrow { return (mState == State.Stopped); }
  @property bool running () @safe const nothrow { return (mState == State.Running); }
  @property bool paused () @safe const nothrow { return (mState == State.Paused); }

  void reset () @trusted nothrow {
    mAccum = Duration.zero;
    mSTime = MonoTime.currTime;
  }

  void start () @trusted {
    if (mState != State.Stopped) throw new Exception("Timer.start(): invalid timer state");
    mAccum = Duration.zero;
    mState = State.Running;
    mSTime = MonoTime.currTime;
  }

  void stop () @trusted {
    if (mState != State.Running) throw new Exception("Timer.stop(): invalid timer state");
    mAccum += MonoTime.currTime-mSTime;
    mState = State.Stopped;
  }

  void pause () @trusted {
    if (mState != State.Running) throw new Exception("Timer.pause(): invalid timer state");
    mAccum += MonoTime.currTime-mSTime;
    mState = State.Paused;
  }

  void resume () @trusted {
    if (mState != State.Paused) throw new Exception("Timer.resume(): invalid timer state");
    mState = State.Running;
    mSTime = MonoTime.currTime;
  }

  string toString () @trusted const {
    import std.string;
    Duration d;
    final switch (mState) {
      case State.Stopped: case State.Paused: d = mAccum; break;
      case State.Running: d = mAccum+(MonoTime.currTime-mSTime); break;
    }
    auto tm = d.split!("hours", "minutes", "seconds", "msecs")();
    if (tm.hours) return format("%s:%02d:%02d.%03d", tm.hours, tm.minutes, tm.seconds, tm.msecs);
    if (tm.minutes) return format("%s:%02d.%03d", tm.minutes, tm.seconds, tm.msecs);
    return format("%s.%03d", tm.seconds, tm.msecs);
  }

private:
  State mState = State.Stopped;
  MonoTime mSTime;
  Duration mAccum;
}
